# CodeTest

Hello, this solution and its corresponding projects are for a team lead coding test.

## Documents For Test [\SuppliedDocuments]
* Clean Architecture
* Development Principles
* SOLID Principles
* Tech Lead Code Test
* Standards ? (Did not receive a standards document)

## Code Limitations
1. Accounted for US zip codes only for validation. This validation should really live in another microservice not embedded in this library
2. No persistence of Settings was setup
3. Error Handling for various HTTP "failure" responses is very limited
4. Not every request and response property TaxJar was implemented