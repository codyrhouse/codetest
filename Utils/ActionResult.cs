﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utils
{
    public class ActionResult
    {
        public bool Success { get; set; }
        public IList<Exception> Exceptions { get; set; } = new List<Exception>();

        public string FlattenedExceptions => Exceptions.Select(e => e.Message).Combine();

        public bool? HasException => Exceptions?.Any();

        public ActionResult(bool success)
        {
            Success = success;
        }

        private ActionResult()
        {

        }

        public ActionResult(Exception ex)
        {
            if (ex == null)
            {
                return; // crh this should be handled in a better way
            }

            Exceptions.Add(ex);
        }

        public ActionResult(string exceptionDetail)
        {
            var result = Guard.IsStringNull(exceptionDetail, nameof(exceptionDetail));

            if (result.AssertionTrue)
            {
                return;
            }

            Exceptions.Add(new Exception(exceptionDetail));
        }

        public static ActionResult Flatten(GuardResultCollector collector)
        {
            if (collector is null)
            {
                throw new ArgumentNullException(nameof(collector));
            }

            var guardResult = collector.GetSummaryResult();

            if (guardResult.AssertionTrue is false)
            {
                return new ActionResult(guardResult.ErrorMessage);
            }

            return new ActionResult(true);
        }

        public static ActionResult Flatten(params ActionResult[] actionResults)
        {
            if (Guard.IsNotPopulated(actionResults, nameof(actionResults)).AssertionTrue)
            {
                throw new ArgumentNullException(nameof(actionResults));
            }

            var flattenedResult = new ActionResult();

            flattenedResult.Success = actionResults.All(a => a.Success);
            flattenedResult.Exceptions = actionResults.SelectMany(a => a.Exceptions).ToList();

            return flattenedResult;
        }

    }
}