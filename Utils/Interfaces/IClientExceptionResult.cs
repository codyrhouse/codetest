﻿namespace Utils.Interfaces
{
    public interface IClientExceptionResult
    {
        string Message { get; }
    }
}
