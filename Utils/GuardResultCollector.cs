﻿using System.Collections.Generic;
using System.Linq;

namespace Utils
{
    public class GuardResultCollector
    {
        private readonly IList<GuardResult> _guardResults = new List<GuardResult>();
        public int ResultCount => _guardResults.Count();

        public void AddGuardCheck(GuardResult result)
        {
            if (Guard.IsNotNull(result, nameof(result)).AssertionTrue)
            {
                _guardResults.Add(result);
            }
        }

        public GuardResult GetSummaryResult()
        {
            var guardResult = new GuardResult();

            guardResult.AssertionTrue = _guardResults.All(g => g.AssertionTrue);
            guardResult.ErrorMessage = _guardResults.Select(g => g.ErrorMessage).Combine();

            return guardResult;
        }

    }
}
