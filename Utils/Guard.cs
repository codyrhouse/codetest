﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Utils
{
    public static class Guard
    {
        public static GuardResult IsZipCodeCorrect(string stringParm, string nameBeingChecked)
        {
            const string pattern = @"(^\d{5}$)|(^\d{9}$)|(^\d{5}-\d{4}$)";

            var isMatch = Regex.Match(stringParm, pattern).Success;

            return isMatch ? GuardResult.True() : GuardResult.False($"{nameBeingChecked} is Invalid");
        }

        public static GuardResult IsStringCorrectLength(string stringParm, string nameBeingChecked, params int[] possibleLengths)
        {
            if (IsStringNull(stringParm, nameBeingChecked).AssertionTrue)
            {
                return GuardResult.False($"{nameBeingChecked} is Empty");
            }

            var length = stringParm.Length;

            var found = possibleLengths.Any(pl => pl == length);
            var combined = possibleLengths.Combine();

            return found ? GuardResult.True() : GuardResult.False($"{nameBeingChecked} [{stringParm}] length is incorrect, must be one of [{combined}]");
        }

        public static GuardResult IsStringNotNull(string stringParm, string nameBeingChecked)
        {
            if (string.IsNullOrEmpty(stringParm))
            {
                return GuardResult.False($"{nameBeingChecked} is empty"); // should go into a resource file for language specific modifications
            }

            return GuardResult.True();
        }

        public static GuardResult IsStringNull(string stringParm, string nameBeingChecked)
        {
            if (string.IsNullOrEmpty(stringParm) is false)
            {
                return GuardResult.False($"{nameBeingChecked} [{stringParm}] is NOT empty");
            }

            return GuardResult.True();
        }

        public static GuardResult IsNotNull(object objectParm, string nameBeingChecked)
        {
            if (objectParm is null)
            {
                return GuardResult.False($"{nameBeingChecked} is NOT empty");
            }

            return GuardResult.True();
        }

        public static GuardResult IsNull(object objectParm, string nameBeingChecked)
        {
            if (objectParm is null)
            {
                return GuardResult.True();
            }

            return GuardResult.False($"{nameBeingChecked} [{objectParm}] is NOT empty");
        }

        public static GuardResult IsPopulated<T>(IEnumerable<T> enumerable, string nameBeingChecked)
        {
            if (enumerable is null || enumerable.Any() is false)
            {
                return GuardResult.False($"{nameBeingChecked} is NOT populated");
            }

            return GuardResult.True();
        }

        public static GuardResult IsNotPopulated<T>(IEnumerable<T> enumerable, string nameBeingChecked)
        {
            if (enumerable is null || enumerable.Any() is false || enumerable.Any(e => e is null))
            {
                return GuardResult.True();
            }

            return GuardResult.False($"{nameBeingChecked} is populated");
        }
        public static GuardResult IsGreaterThanZero(decimal toBeChecked, string nameBeingChecked)
        {
            return toBeChecked > 0 ? GuardResult.True() : GuardResult.False($"{nameBeingChecked} Must Be Greater Than Zero");
        }
    }
}
