﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utils
{
    public static class Extensions
    {
        public static string Combine(this IEnumerable<string> toBeCombined)
        {
            return toBeCombined.Where(t => string.IsNullOrEmpty(t) is false).Aggregate(new StringBuilder(), (current, next) => current.Append(current.Length == 0 ? string.Empty : ", ").Append(next)).ToString();
        }

        public static string Combine(this IEnumerable<int> toBeCombined)
        {
            return toBeCombined.Select(t => t.ToString()).Combine();
        }
    }
}
