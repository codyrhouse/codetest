﻿using Flurl;
using System;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Utils.Interfaces;

namespace Utils
{
    public static class HttpClientHelper
    {
        public static HttpClient CreateClientForJson(string bearerToken = "")
        {
            var client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (string.IsNullOrEmpty(bearerToken) is false)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", bearerToken);
            }

            return client;
        }

        public static void ThrowOnFailure<T>(HttpResponseMessage message) where T : IClientExceptionResult
        {
            if (message.IsSuccessStatusCode)
            {
                return;
            }

            T error = default;

            try
            {
                error = message.Content.ReadAsAsync<T>().GetAwaiter().GetResult();
            }
            catch (Exception)
            {
                throw new Exception($"{message.StatusCode} Error Occurred when Parsing Response");
            }

            throw new Exception($"{message.StatusCode} {error.Message}");

        }

        public static T1 GetJson<T1, T2>(string url, string bearerToken = "") where T2 : IClientExceptionResult
        {
            using var client = CreateClientForJson(bearerToken);
            // crh: this is necessary only because the rest of the developer test is not coded for async in order to keep it simple
            var message = client.GetAsync(url).GetAwaiter().GetResult();

            ThrowOnFailure<T2>(message);

            var deserialized = message.Content.ReadAsAsync<T1>().GetAwaiter().GetResult();

            return deserialized;

        }

        public static T3 PostJson<T1, T2, T3>(T1 toBePosted, string url, string bearerToken = "") where T2 : IClientExceptionResult
        {
            using var client = CreateClientForJson(bearerToken);
            // crh: this is necessary only because the rest of the developer test is not coded for async in order to keep it simple
            var message = client.PostAsJsonAsync(url, toBePosted).GetAwaiter().GetResult();

            ThrowOnFailure<T2>(message);

            var deserialized = message.Content.ReadAsAsync<T3>().GetAwaiter().GetResult();

            return deserialized;

        }

        public static string BuildUrlWithQueryStrings(string baseUrl, string extraPath, Action<NameValueCollection> addQueryStrings)
        {
            var builtUrl = Url.Combine(baseUrl, extraPath);
            var builder = new UriBuilder(builtUrl);

            var query = HttpUtility.ParseQueryString(builder.Query);

            addQueryStrings(query);

            // ReSharper disable once AssignNullToNotNullAttribute
            builder.Query = query.ToString();

            return builder.ToString();
        }
    }
}
