﻿namespace Utils
{
    public class GuardResult
    {
        public bool AssertionTrue { get; set; }

        public string ErrorMessage { get; set; }

        public static GuardResult True()
        {
            return new GuardResult() { AssertionTrue = true };
        }

        public static GuardResult False(string errorMessage)
        {
            return new GuardResult() { ErrorMessage = errorMessage };
        }
    }
}
