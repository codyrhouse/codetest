﻿namespace Utils
{
    public static class Sanitizer
    {
        public static string CleanString(string stringParm, string nameBeingChecked)
        {
            if (Guard.IsStringNotNull(stringParm, nameBeingChecked).AssertionTrue)
            {
                return stringParm.Trim();
            }

            return stringParm;
        }
    }
}
