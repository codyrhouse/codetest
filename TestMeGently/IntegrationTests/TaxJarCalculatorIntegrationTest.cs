﻿using CodeTest.Calculators;
using CodeTest.Pocos;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace TestMeGently.IntegrationTests
{
    [ExcludeFromCodeCoverage]
    public class TaxJarCalculatorIntegrationTest
    {
        [Fact]
        public void TotalTaxesWagonerOK()
        {
            // arrange
            const decimal expectedTaxRate = 0.098m;
            const decimal orderTotalNoShipping = 100;
            const decimal expectedCalculatedTaxes = 9.8m;
            const decimal expectedTotalOrder = 109.80m;

            var calculator = new TaxJarCalculator();
            var location = new GenericLocation();

            location.City = "Wagoner";
            location.State = "OK";
            location.Zip = "74467";
            location.Country = "US";

            // act
            var (actionResult, taxRate) = calculator.TotalTaxesForOrder(location, orderTotalNoShipping);

            // assert
            Assert.True(actionResult.Success, actionResult.FlattenedExceptions);
            Assert.Equal(expectedTaxRate, taxRate.TaxRate);
            Assert.Equal(expectedCalculatedTaxes, taxRate.OrderTaxes);
            Assert.Equal(expectedTotalOrder, taxRate.OrderGrossNoShippingWithTaxes);
        }

        [Fact]
        public void RateWagonerOK()
        {
            // arrange
            const decimal expectedTaxRate = 0.098m;

            var calculator = new TaxJarCalculator();
            var location = new GenericLocation();

            location.City = "Wagoner";
            location.State = "OK";
            location.Zip = "74467";
            location.Country = "US";

            // act
            var (actionResult, taxRate) = calculator.TaxRateByLocation(location);

            // assert
            Assert.True(actionResult.Success, actionResult.FlattenedExceptions);
            Assert.Equal(expectedTaxRate, taxRate);

        }
    }
}
