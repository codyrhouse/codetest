using System;
using CodeTest.EndpointClients.TaxJar;
using CodeTest.Pocos;
using CodeTest.Pocos.Locations;
using System.Diagnostics.CodeAnalysis;
using Utils;
using Xunit;

namespace TestMeGently.IntegrationTests
{
    [ExcludeFromCodeCoverage]
    public class TaxJarHttpClientIntegrationTest
    {
        [Fact]
        public void RateWagonerOK()
        {
            // arrange
            const decimal expectedTaxRate = 0.098m;

            var client = new TaxJarHttpClient(Settings.TaxJarUrl);
            var location = new TaxJarLocation();

            location.Zip = "74467";

            // act

            var validResult = location.IsValid();
            var (actionResult, taxRate) = client.TaxRateByLocation(location);

            // assert
            Assert.True(validResult.Success);
            Assert.True(actionResult.Success);
            Assert.Equal(expectedTaxRate, taxRate);
        }

        [Fact]
        public void RateWagonerOKInvalidZip()
        {
            // arrange
            var client = new TaxJarHttpClient(Settings.TaxJarUrl);
            var location = new TaxJarLocation();

            location.Zip = "7446";

            // act

            var validResult = location.IsValid();
            var (actionResult, taxRate) = client.TaxRateByLocation(location);

            // assert
            Assert.False(validResult.Success);
            Assert.False(actionResult.Success);
        }

        [Fact]
        public void RateWagonerOKWithGeneric()
        {
            // arrange
            const decimal expectedTaxRate = 0.098m;

            var client = new TaxJarHttpClient(Settings.TaxJarUrl);
            var location = new GenericLocation();

            location.Zip = "74467";

            // act
            var (actionResult, taxRate) = client.TaxRateByLocation(location);

            // assert
            Assert.True(actionResult.Success);
            Assert.Equal(expectedTaxRate, taxRate);
        }

        [Fact]
        public void TotalTaxesForOrderWagonerMissingFields()
        {
            // arrange
            const decimal orderTotalNoShipping = 100;

            var client = new TaxJarHttpClient(Settings.TaxJarUrl);
            var location = new TaxJarLocation();

            location.Zip = "74467";

            // act
            var (actionResult, _) = client.TotalTaxesForOrder(location, orderTotalNoShipping);

            // assert
            Assert.False(actionResult.Success);

        }

        [Fact]
        public void TotalTaxesForOrderWagonerSuccess()
        {
            // arrange
            const decimal orderTotalNoShipping = 100;
            const decimal expectedToCollect = 9.8m;
            const decimal expectedRate = 0.098m;

            var client = new TaxJarHttpClient(Settings.TaxJarUrl);
            var location = new TaxJarLocation();

            location.City = "Wagoner";
            location.State = "OK";
            location.Zip = "74467";
            location.Country = "US";

            // act
            var (actionResult, totalTaxes) = client.TotalTaxesForOrder(location, orderTotalNoShipping);

            // assert
            Assert.True(actionResult.Success);
            Assert.Equal(expectedToCollect, totalTaxes.AmountToCollect);
            Assert.Equal(expectedRate, totalTaxes.Rate);
        }

        [Fact]
        public void TotalTaxesForOrderWagonerWrongState()
        {
            // arrange
            const decimal orderTotalNoShipping = 100;

            var client = new TaxJarHttpClient(Settings.TaxJarUrl);
            var location = new TaxJarLocation();

            location.City = "Wagoner";
            location.State = "TN";
            location.Zip = "74467";
            location.Country = "US";

            // act

            // assert
            Assert.Throws<Exception>(() =>
            {
                client.TotalTaxesForOrder(location, orderTotalNoShipping);
            });
        }
    }
}
