﻿using CodeTest.Interfaces;
using CodeTest.Pocos;
using System;
using Utils;

namespace TestMeGently.FauxObjects
{
    public class FauxSuccess10PercentCalculator : ITaxCalculator
    {
        public (ActionResult ActionResult, decimal TaxRate) TaxRateByLocation(GenericLocation genericLocation)
        {
            return (new ActionResult(true), 0.98m);
        }

        public (ActionResult TotalTaxesActionResult, TotalCalculatedTaxes TotalTaxes) TotalTaxesForOrder(GenericLocation genericLocation, decimal orderGrossNoShipping)
        {
            var taxes = new TotalCalculatedTaxes();

            taxes.TaxRate = 0.1m;
            taxes.OrderGrossNoShipping = orderGrossNoShipping;
            taxes.OrderTaxes = Math.Round(taxes.TaxRate * orderGrossNoShipping, 2);

            return (new ActionResult(true), taxes);
        }
    }
}
