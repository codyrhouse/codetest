﻿using CodeTest.Interfaces;
using CodeTest.Pocos;
using System;
using Utils;

namespace TestMeGently.FauxObjects
{
    public class FauxFailureThrowsCalculator : ITaxCalculator
    {
        public (ActionResult ActionResult, decimal TaxRate) TaxRateByLocation(GenericLocation genericLocation)
        {
            throw new ArgumentException("yep, this is the start of an argument");
        }

        public (ActionResult TotalTaxesActionResult, TotalCalculatedTaxes TotalTaxes) TotalTaxesForOrder(GenericLocation genericLocation, decimal orderGrossNoShipping)
        {
            throw new ArgumentException("yep, this is the start of an argument");
        }
    }
}
