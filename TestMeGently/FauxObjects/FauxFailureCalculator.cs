﻿using CodeTest.Interfaces;
using CodeTest.Pocos;
using Utils;

namespace TestMeGently.FauxObjects
{
    public class FauxFailureCalculator : ITaxCalculator
    {
        public (ActionResult ActionResult, decimal TaxRate) TaxRateByLocation(GenericLocation genericLocation)
        {
            return (new ActionResult("bad data, fix it"), decimal.MinusOne);
        }

        public (ActionResult TotalTaxesActionResult, TotalCalculatedTaxes TotalTaxes) TotalTaxesForOrder(GenericLocation genericLocation, decimal orderGrossNoShipping)
        {
            var taxes = new TotalCalculatedTaxes();

            taxes.TaxRate = 0.1m;
            taxes.OrderGrossNoShipping = 100;

            return (new ActionResult("bad data, fix it"), taxes);
        }
    }
}
