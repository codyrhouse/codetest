﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace TestMeGently.Pocos
{
    [ExcludeFromCodeCoverage]
    public class PokeBerryFlavorAttribute
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
