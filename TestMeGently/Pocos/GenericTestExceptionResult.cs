﻿using System.Diagnostics.CodeAnalysis;
using Utils.Interfaces;

namespace TestMeGently.Pocos
{
    [ExcludeFromCodeCoverage]
    public class GenericTestExceptionResult : IClientExceptionResult
    {
        public string Message => "Hi this is a test";
    }
}
