﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace TestMeGently.Pocos
{
    [ExcludeFromCodeCoverage]
    public class PokeBerry
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("flavors")]
        public List<PokeBerryFlavor> Flavors { get; set; }
    }
}
