﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace TestMeGently.Pocos
{
    [ExcludeFromCodeCoverage]
    public class PokeBerryFlavor
    {
        [JsonProperty("potency")]
        public int Potency { get; set; }
        [JsonProperty("flavor")]
        public PokeBerryFlavorAttribute Flavor { get; set; }
    }
}
