using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Http;
using TestMeGently.Pocos;
using Utils;
using Xunit;

namespace TestMeGently.UnitTests
{

    [ExcludeFromCodeCoverage]
    public class HttpClientHelperTest
    {
        [Fact]
        public void CreateClient()
        {
            // arrange

            // act
            var client = HttpClientHelper.CreateClientForJson();

            // assert
            Assert.NotNull(client);
            Assert.Single(client.DefaultRequestHeaders);
        }

        [Fact]
        public void GetJsonNullUrl()
        {
            // arrange

            // act

            // assert
            Assert.Throws<InvalidOperationException>(() => HttpClientHelper.GetJson<PokeBerry, GenericTestExceptionResult>(string.Empty));
        }

        [Fact]
        public void GetJsonPokeApi()
        {
            // arrange
            const string url = "https://pokeapi.co/api/v2/berry/cheri/";
            const string expectedName = "cheri";
            const int expectedFlavorCount = 5;

            // act
            var berry = HttpClientHelper.GetJson<PokeBerry, GenericTestExceptionResult>(url);

            // assert
            Assert.Equal(expectedName, berry.Name);
            Assert.Equal(expectedFlavorCount, berry.Flavors.Count);

        }

        [Fact]
        public void GetJsonPokeApiNotFound()
        {
            // arrange
            const string url = "https://pokeapi.co/api/v2/berry/cherixxx/";

            // act

            // assert

            Assert.Throws<Exception>(() => HttpClientHelper.GetJson<PokeBerry, GenericTestExceptionResult>(url));

        }

        [Fact]
        public void BuildUrl()
        {
            // arrange
            const string url = "https://pokeapi.co/api/v2/berry";
            var expectedUrlPrefix = url.Substring(0, 10);

            // act
            var builtUrl = HttpClientHelper.BuildUrlWithQueryStrings(url, "cheri", (query) =>
            {
                query["firstname"] = "Iron";
                query["lastname"] = "Man";
            });

            // assert
            Assert.NotNull(builtUrl);
            Assert.Contains(expectedUrlPrefix, builtUrl);
            Assert.Contains("cheri", builtUrl);
            Assert.Contains("firstname=", builtUrl);
            Assert.Contains("lastname=", builtUrl);

        }

        [Fact]
        public void ThrowOnFailureNotFound()
        {
            // arrange
            var message = new HttpResponseMessage();

            message.StatusCode = HttpStatusCode.NotFound;
            message.Content = new StringContent("this is a test");

            // act


            // assert
            Assert.Throws<Exception>(() =>
            {
                HttpClientHelper.ThrowOnFailure<GenericTestExceptionResult>(message);
            });

        }

        [Fact]
        public void ThrowOnFailureSuccess()
        {
            // arrange
            var message = new HttpResponseMessage();

            message.StatusCode = HttpStatusCode.OK;
            message.Content = new StringContent("this is a test");

            // act
            HttpClientHelper.ThrowOnFailure<GenericTestExceptionResult>(message);

            // assert
            // this test *should not* throw an exception
            Assert.Equal(HttpStatusCode.OK, message.StatusCode);
        }
    }
}
