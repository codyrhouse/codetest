using CodeTest.EndpointClients.TaxJar;
using CodeTest.Pocos.Locations;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace TestMeGently.UnitTests
{
    [ExcludeFromCodeCoverage]
    public class TaxJarHttpClientTest
    {
        [Fact]
        public void EmptyBaseUrl()
        {
            // arrange

            // act

            // assert
            Assert.Throws<ArgumentNullException>(() => new TaxJarHttpClient(string.Empty));
        }
        
    }
}
