using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime;
using Utils;
using Xunit;

namespace TestMeGently.UnitTests
{
    [ExcludeFromCodeCoverage]
    public class ActionResultTest
    {
        [Fact]
        public void NullException()
        {
            // arrange
            const int expectedCount = 0;
            Exception ex = null;

            // act
            // ReSharper disable once ExpressionIsAlwaysNull
            var ar = new Utils.ActionResult(ex);

            // assert
            Assert.Equal(expectedCount, ar.Exceptions.Count);
            Assert.False(ar.HasException);
        }

        [Fact]
        public void OneException()
        {
            // arrange
            const int expectedCount = 1;

            // act
            // ReSharper disable once ExpressionIsAlwaysNull
            var ar = new Utils.ActionResult(new ArgumentNullException());

            // assert
            Assert.Equal(expectedCount, ar.Exceptions.Count);
            Assert.True(ar.HasException);
        }

        [Fact]
        public void TwoExceptions()
        {
            // arrange
            const int expectedCount = 2;

            // act
            // ReSharper disable once ExpressionIsAlwaysNull
            var ar = new Utils.ActionResult(new ArgumentNullException());

            ar.Exceptions.Add(new AmbiguousImplementationException());

            // assert
            Assert.Equal(expectedCount, ar.Exceptions.Count);
            Assert.True(ar.HasException);
        }

        [Fact]
        public void NullExceptionWithText()
        {
            // arrange
            const int expectedCount = 0;

            // act
            var ar = new Utils.ActionResult(string.Empty);

            // assert
            Assert.Equal(expectedCount, ar.Exceptions.Count);
            Assert.False(ar.HasException);
            Assert.Empty(ar.FlattenedExceptions);
        }

        [Fact]
        public void NullExceptionWithText2()
        {
            // arrange
            const int expectedCount = 0;
            string nullText = null;

            // act
            // ReSharper disable once ExpressionIsAlwaysNull
            var ar = new Utils.ActionResult(nullText);

            // assert
            Assert.Equal(expectedCount, ar.Exceptions.Count);
            Assert.False(ar.HasException);
        }

        [Fact]
        public void OneExceptionWithText()
        {
            // arrange
            const int expectedCount = 1;

            // act
            var ar = new Utils.ActionResult("don't mix your apples and oranges");

            // assert
            Assert.Equal(expectedCount, ar.Exceptions.Count);
            Assert.True(ar.HasException);
            Assert.NotEmpty(ar.FlattenedExceptions);
        }

        [Fact]
        public void TwoExceptionsWithText()
        {
            // arrange
            const int expectedCount = 2;

            // act
            // ReSharper disable once ExpressionIsAlwaysNull
            var ar = new Utils.ActionResult("seriously don't mix them");

            ar.Exceptions.Add(new AmbiguousImplementationException());

            // assert
            Assert.Equal(expectedCount, ar.Exceptions.Count);
            Assert.True(ar.HasException);
        }

        [Fact]
        public void FlattenEmpty()
        {
            // arrange
            GuardResultCollector collector = null;

            // act


            // assert
            // ReSharper disable once ExpressionIsAlwaysNull
            Assert.Throws<ArgumentNullException>(() => Utils.ActionResult.Flatten(collector));

        }

        [Fact]
        public void FlattenSingleSuccess()
        {
            // arrange
            var collector = new GuardResultCollector();
            const int expectedCount = 1;

            // act
            collector.AddGuardCheck(Utils.Guard.IsNull(null, "null parameter"));
            var flattenedResult = Utils.ActionResult.Flatten(collector);

            // assert
            Assert.Equal(expectedCount, collector.ResultCount);
            Assert.NotNull(flattenedResult);
            Assert.False(flattenedResult.HasException);
            Assert.True(flattenedResult.Success);

        }

        [Fact]
        public void FlattenSingleFailure()
        {
            // arrange
            var collector = new GuardResultCollector();
            const int expectedCount = 1;

            // act
            collector.AddGuardCheck(Guard.IsNull("Monkeys love bananas", "And sticks"));
            var flattenedResult = ActionResult.Flatten(collector);

            // assert
            Assert.Equal(expectedCount, collector.ResultCount);
            Assert.NotNull(flattenedResult);
            Assert.True(flattenedResult.HasException);
            Assert.False(flattenedResult.Success);

        }

        [Fact]
        public void FlattenDoubleSuccess()
        {
            // arrange
            var collector = new GuardResultCollector();
            const int expectedCount = 2;

            // act
            collector.AddGuardCheck(Utils.Guard.IsNull(null, "tester"));
            collector.AddGuardCheck(Utils.Guard.IsNull(null, "tester"));

            var flattenedResult = Utils.ActionResult.Flatten(collector);

            // assert
            Assert.Equal(expectedCount, collector.ResultCount);
            Assert.NotNull(flattenedResult);
            Assert.False(flattenedResult.HasException);
            Assert.True(flattenedResult.Success);

        }

        [Fact]
        public void FlattenDoubleChecksSingleFailure()
        {
            // arrange
            var collector = new GuardResultCollector();
            const int expectedCount = 2;
            const string expectedMessage = "Monkeys love bananas";

            // act
            collector.AddGuardCheck(Utils.Guard.IsNull(null, "tester"));
            collector.AddGuardCheck(Utils.Guard.IsNull(expectedMessage, nameof(expectedMessage)));
            var flattenedResult = Utils.ActionResult.Flatten(collector);

            // assert
            Assert.Equal(expectedCount, collector.ResultCount);
            Assert.NotNull(flattenedResult);
            Assert.True(flattenedResult.HasException);
            Assert.False(flattenedResult.Success);

        }

        [Fact]
        public void SetExceptions()
        {
            // arrange
            const int expectedCount = 0;

            // act
            var ar = new Utils.ActionResult(true);
            ar.Exceptions = new List<Exception>();

            // assert
            Assert.Equal(expectedCount, ar.Exceptions.Count);


        }

        [Fact]
        public void SetExceptionsToNull()
        {
            // arrange

            // act
            var ar = new Utils.ActionResult(true);
            ar.Exceptions = null;

            // assert
            Assert.Throws<NullReferenceException>(() => ar.Exceptions.Count);

        }

        [Fact]
        public void FlattenParamsFailure()
        {
            // arrange
            Utils.ActionResult ar = null;

            // act

            // assert
            Assert.Throws<ArgumentNullException>(() => Utils.ActionResult.Flatten(ar));

        }

        [Fact]
        public void FlattenParamsSuccess()
        {
            // arrange
            var ar = new Utils.ActionResult(true);

            // act
            var flattenedResult = Utils.ActionResult.Flatten(ar);

            // assert
            Assert.NotNull(flattenedResult);
            Assert.True(flattenedResult.Success);

        }
    }
}
