using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace TestMeGently.UnitTests
{
    [ExcludeFromCodeCoverage]
    public class GuardTest
    {
        [Fact]
        public void ValidZip()
        {
            // arrange
            const string zip = "37363";

            // act
            var gr = Utils.Guard.IsZipCodeCorrect(zip, nameof(zip));

            // assert
            Assert.True(gr.AssertionTrue);
            Assert.Null(gr.ErrorMessage);
        }

        [Fact]
        public void ValidZip2()
        {
            // arrange
            const string zip = "74467-6789";

            // act
            var gr = Utils.Guard.IsZipCodeCorrect(zip, nameof(zip));

            // assert
            Assert.True(gr.AssertionTrue);
            Assert.Null(gr.ErrorMessage);
        }

        [Fact]
        public void ValidStringLengthSingleParam()
        {
            // arrange
            var toCheck = "123";

            // act
            var gr = Utils.Guard.IsStringCorrectLength(toCheck, nameof(toCheck), 3);

            // assert
            Assert.True(gr.AssertionTrue);
            Assert.Null(gr.ErrorMessage);
        }

        [Fact]
        public void ValidStringLengthDoubleParam()
        {
            // arrange
            var toCheck = "123";

            // act
            var gr = Utils.Guard.IsStringCorrectLength(toCheck, nameof(toCheck), 3, 10);

            // assert
            Assert.True(gr.AssertionTrue);
            Assert.Null(gr.ErrorMessage);
        }

        [Fact]
        public void InvalidStringLengthSingleParam()
        {
            // arrange
            var toCheck = "123";

            // act
            var gr = Utils.Guard.IsStringCorrectLength(toCheck, nameof(toCheck), 2);

            // assert
            Assert.False(gr.AssertionTrue);
            Assert.NotNull(gr.ErrorMessage);
        }

        [Fact]
        public void InvalidStringLengthDoubleParam()
        {
            // arrange
            var toCheck = "123";

            // act
            var gr = Utils.Guard.IsStringCorrectLength(toCheck, nameof(toCheck), 2, 10);

            // assert
            Assert.False(gr.AssertionTrue);
            Assert.NotNull(gr.ErrorMessage);
        }

        [Fact]
        public void InvalidStringLengthNull()
        {
            // arrange
            string toCheck = null;

            // act
            // ReSharper disable once ExpressionIsAlwaysNull
            var gr = Utils.Guard.IsStringCorrectLength(toCheck, nameof(toCheck), 2);

            // assert
            Assert.False(gr.AssertionTrue);
            Assert.NotNull(gr.ErrorMessage);
        }

        [Fact]
        public void InvalidStringNotNull()
        {
            // arrange
            string toCheck = null;

            // act
            // ReSharper disable once ExpressionIsAlwaysNull
            var gr = Utils.Guard.IsStringNotNull(toCheck, nameof(toCheck));

            // assert
            Assert.False(gr.AssertionTrue);
            Assert.NotNull(gr.ErrorMessage);
        }

        [Fact]
        public void ValidStringNotNull()
        {
            // arrange
            const string toCheck = "123";

            // act
            // ReSharper disable once ExpressionIsAlwaysNull
            var gr = Utils.Guard.IsStringNotNull(toCheck, nameof(toCheck));

            // assert
            Assert.True(gr.AssertionTrue);
            Assert.Null(gr.ErrorMessage);
        }

        [Fact]
        public void InvalidStringNull()
        {
            // arrange
            string toCheck = null;

            // act
            // ReSharper disable once ExpressionIsAlwaysNull
            var gr = Utils.Guard.IsStringNull(toCheck, nameof(toCheck));

            // assert
            Assert.True(gr.AssertionTrue);
            Assert.Null(gr.ErrorMessage);
        }

        [Fact]
        public void ValidStringNull()
        {
            // arrange
            const string toCheck = "123";

            // act
            var gr = Utils.Guard.IsStringNull(toCheck, nameof(toCheck));

            // assert
            Assert.False(gr.AssertionTrue);
            Assert.NotNull(gr.ErrorMessage);
        }

        [Fact]
        public void ValidObjectNotNull()
        {
            // arrange
            const int toCheck = 123;

            // act
            var gr = Utils.Guard.IsNotNull(toCheck, nameof(toCheck));

            // assert
            Assert.True(gr.AssertionTrue);
            Assert.Null(gr.ErrorMessage);
        }

        [Fact]
        public void InvalidObjectNotNull()
        {
            // arrange
            const object toCheck = null;

            // act
            var gr = Utils.Guard.IsNotNull(toCheck, nameof(toCheck));

            // assert
            Assert.False(gr.AssertionTrue);
            Assert.NotNull(gr.ErrorMessage);
        }

        [Fact]
        public void InvalidObjectNull()
        {
            // arrange
            const int toCheck = 123;

            // act
            var gr = Utils.Guard.IsNull(toCheck, nameof(toCheck));

            // assert
            Assert.False(gr.AssertionTrue);
            Assert.NotNull(gr.ErrorMessage);
        }

        [Fact]
        public void ValidObjectNull()
        {
            // arrange
            const object toCheck = null;

            // act
            var gr = Utils.Guard.IsNull(toCheck, nameof(toCheck));

            // assert
            Assert.True(gr.AssertionTrue);
            Assert.Null(gr.ErrorMessage);
        }

        [Fact]
        public void InvalidPopulatedEnumerable()
        {
            // arrange
            // ReSharper disable once CollectionNeverUpdated.Local
            var toCheck = new List<string>();

            // act
            var gr = Utils.Guard.IsPopulated(toCheck, nameof(toCheck));

            // assert
            Assert.False(gr.AssertionTrue);
            Assert.NotNull(gr.ErrorMessage);
        }

        [Fact]
        public void ValidPopulatedEnumerable()
        {
            // arrange
            // ReSharper disable once CollectionNeverUpdated.Local
            var toCheck = new List<string> { "tiger" };

            // act
            var gr = Utils.Guard.IsPopulated(toCheck, nameof(toCheck));

            // assert
            Assert.True(gr.AssertionTrue);
            Assert.Null(gr.ErrorMessage);
        }

        [Fact]
        public void InvalidNotPopulatedEnumerable()
        {
            // arrange
            // ReSharper disable once CollectionNeverUpdated.Local
            var toCheck = new List<string> { "tiger" };

            // act
            var gr = Utils.Guard.IsNotPopulated(toCheck, nameof(toCheck));

            // assert
            Assert.False(gr.AssertionTrue);
            Assert.NotNull(gr.ErrorMessage);
        }

        [Fact]
        public void ValidNotPopulatedEnumerable()
        {
            // arrange
            // ReSharper disable once CollectionNeverUpdated.Local
            var toCheck = new List<string>();

            // act
            var gr = Utils.Guard.IsNotPopulated(toCheck, nameof(toCheck));

            // assert
            Assert.True(gr.AssertionTrue);
            Assert.Null(gr.ErrorMessage);
        }

        [Fact]
        public void GreaterThanZeroSuccess()
        {
            // arrange
            const int toCheck = 123;

            // act
            var gr = Utils.Guard.IsGreaterThanZero(toCheck, nameof(toCheck));

            // assert
            Assert.True(gr.AssertionTrue);
        }

        [Fact]
        public void GreaterThanZeroFailure()
        {
            // arrange
            const int toCheck = 0;

            // act
            var gr = Utils.Guard.IsGreaterThanZero(toCheck, nameof(toCheck));

            // assert
            Assert.False(gr.AssertionTrue);
            Assert.NotNull(gr.ErrorMessage);
        }
    }
}
