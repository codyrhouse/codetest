using CodeTest.Pocos;
using CodeTest.Pocos.Locations;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace TestMeGently.UnitTests
{
    [ExcludeFromCodeCoverage]
    public class PocosTest
    {
        [Fact]
        public void TaxJarLocationValid()
        {
            // arrange
            var location = new TaxJarLocation { Zip = "74467" };

            // act
            location.SanitizeData();
            var ar = location.IsValid();

            // assert
            Assert.True(ar.Success);
        }

        [Fact]
        public void TaxJarLocationInvalid()
        {
            // arrange
            var location = new TaxJarLocation { Zip = "7446" };

            // act
            location.SanitizeData();
            var ar = location.IsValid();

            // assert
            Assert.False(ar.Success);
        }

        [Fact]
        public void TaxJarLocationConvertFrom()
        {
            // arrange
            var generic = new GenericLocation();

            generic.Zip = "74467";

            // act
            var location = TaxJarLocation.ConvertFrom(generic);

            location.SanitizeData();
            var ar = location.IsValid();

            // assert
            Assert.True(ar.Success);
        }

        [Fact]
        public void TaxJarLocationConvertFromThrows()
        {
            // arrange
            var generic = new GenericLocation();

            // act
            var location = TaxJarLocation.ConvertFrom(generic);

            location.SanitizeData();

            // assert
            Assert.Throws<ArgumentNullException>(() => location.IsValid());

        }

        [Fact]
        public void CalculatedTax10Percent()
        {
            // arrange
            var taxes = new TotalCalculatedTaxes();
            const decimal expectedTotalTaxes = 10m;
            const decimal expectedTotalOrderWithTaxes = 110m;

            // act
            taxes.TaxRate = 0.1m;
            taxes.OrderGrossNoShipping = 100m;
            taxes.OrderTaxes = Math.Round(taxes.TaxRate * taxes.OrderGrossNoShipping);

            // assert
            Assert.Equal(expectedTotalTaxes, taxes.OrderTaxes);
            Assert.Equal(expectedTotalOrderWithTaxes, taxes.OrderGrossNoShippingWithTaxes);

        }

        [Fact]
        public void TotalCalculatedTaxesEmpty()
        {
            // arrange
            var taxes = TotalCalculatedTaxes.Empty;
            const decimal expectedTotalTaxes = 0;
            const decimal expectedTotalOrderWithTaxes = 0;

            // act

            // assert
            Assert.Equal(expectedTotalTaxes, taxes.OrderTaxes);
            Assert.Equal(expectedTotalOrderWithTaxes, taxes.OrderGrossNoShippingWithTaxes);

        }

        [Fact]
        public void GenericLocation1()
        {
            // arrange
            var location = new GenericLocation();

            const string expectedCity = "Chattanooga";
            const string expectedStreet = "Main Street";
            const string expectedState = "TN";
            const string expectedZip = "37421";
            const string expectedCountry = "US";

            // act
            location.City = expectedCity;
            location.Street = expectedStreet;
            location.State = expectedState;
            location.Zip = expectedZip;
            location.Country = expectedCountry;

            // assert
            Assert.Equal(expectedCity, location.City);
            Assert.Equal(expectedStreet, location.Street);
            Assert.Equal(expectedState, location.State);
            Assert.Equal(expectedZip, location.Zip);
            Assert.Equal(expectedCountry, location.Country);

        }
    }
}
