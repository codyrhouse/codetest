using CodeTest;
using CodeTest.Pocos;
using System;
using System.Diagnostics.CodeAnalysis;
using TestMeGently.FauxObjects;
using Xunit;

namespace TestMeGently.UnitTests
{
    [ExcludeFromCodeCoverage]
    public class TaxServiceTest
    {
        [Fact]
        public void FauxCalculatorSuccess()
        {
            // arrange
            var fauxCalculator = new FauxSuccess10PercentCalculator();
            var taxService = new TaxService(fauxCalculator);

            const decimal expectedOrderGross = 100m;
            const decimal expectedTaxRate = 0.1m;
            const decimal expectedOrderTaxes = 10m;
            const decimal expectedOrderGrossWithTaxes = 110m;

            // act
            var (totalTaxesForOrderResult, totalTaxes) = taxService.TotalTaxesForOrder(new GenericLocation(), expectedOrderGross);

            // assert
            Assert.True(totalTaxesForOrderResult.Success);

            Assert.Equal(expectedOrderGross, totalTaxes.OrderGrossNoShipping);
            Assert.Equal(expectedTaxRate, totalTaxes.TaxRate);

            Assert.Equal(expectedOrderTaxes, totalTaxes.OrderTaxes);
            Assert.Equal(expectedOrderGrossWithTaxes, totalTaxes.OrderGrossNoShippingWithTaxes);

        }

        [Fact]
        public void FauxCalculatorFailure()
        {
            // arrange
            var fauxCalculator = new FauxFailureCalculator();
            var taxService = new TaxService(fauxCalculator);

            const decimal expectedOrderGross = 100m;

            // act
            var (totalTaxesForOrderResult, totalTaxes) = taxService.TotalTaxesForOrder(new GenericLocation(), expectedOrderGross);

            // assert
            Assert.False(totalTaxesForOrderResult.Success);
            Assert.Equal(expectedOrderGross, totalTaxes.OrderGrossNoShipping);
        }

        [Fact]
        public void FauxCalculatorFailureNullCalculator()
        {
            // arrange
            FauxFailureCalculator fauxCalculator = null;

            // act

            // assert
            Assert.Throws<ArgumentNullException>(() => new TaxService(fauxCalculator));
        }

        [Fact]
        public void FauxCalculatorTotalTaxesFailureThrowsInternalException()
        {
            // arrange
            var fauxCalculator = new FauxFailureThrowsCalculator();
            var taxService = new TaxService(fauxCalculator);

            // act
            var totalTaxes = taxService.TotalTaxesForOrder(null, 1);

            // assert
            Assert.False(totalTaxes.TotalTaxesForOrderResult.Success);
        }

        [Fact]
        public void FauxCalculatorRateFailureThrowsInternalException()
        {
            // arrange
            var fauxCalculator = new FauxFailureThrowsCalculator();
            var taxService = new TaxService(fauxCalculator);

            // act
            var totalTaxes = taxService.TaxRateByLocation(null);

            // assert
            Assert.False(totalTaxes.ActionResult.Success);
        }

        [Fact]
        public void FauxCalculatorRateSuccess10Percent()
        {
            // arrange
            var fauxCalculator = new FauxSuccess10PercentCalculator();
            var taxService = new TaxService(fauxCalculator);

            // act
            var totalTaxes = taxService.TaxRateByLocation(null);

            // assert
            Assert.True(totalTaxes.ActionResult.Success);
        }

        [Fact]
        public void FauxCalculatorRateFailure()
        {
            // arrange
            var fauxCalculator = new FauxFailureCalculator();
            var taxService = new TaxService(fauxCalculator);

            // act
            var totalTaxes = taxService.TaxRateByLocation(null);

            // assert
            Assert.False(totalTaxes.ActionResult.Success);
        }

    }
}
