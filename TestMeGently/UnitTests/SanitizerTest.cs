using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace TestMeGently.UnitTests
{
    [ExcludeFromCodeCoverage]
    public class SanitizerTest
    {
        [Fact]
        public void CleanString1()
        {
            // arrange
            const string toBeCleaned = " abc ";
            var expectedClean = toBeCleaned.Trim();

            // act
            var actualCleaned = Utils.Sanitizer.CleanString(toBeCleaned, nameof(toBeCleaned));

            // assert
            Assert.Equal(expectedClean, actualCleaned);
        }

        [Fact]
        public void CleanString2()
        {
            // arrange
            const string toBeCleaned = null;
            const string expectedClean = null;

            // act
            var actualCleaned = Utils.Sanitizer.CleanString(toBeCleaned, nameof(toBeCleaned));

            // assert
            Assert.Equal(expectedClean, actualCleaned);
        }
    }
}
