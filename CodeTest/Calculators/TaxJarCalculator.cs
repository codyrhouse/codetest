﻿using CodeTest.EndpointClients.TaxJar;
using CodeTest.Interfaces;
using CodeTest.Pocos;
using CodeTest.Pocos.Locations;
using Utils;

namespace CodeTest.Calculators
{
    public class TaxJarCalculator : ITaxCalculator
    {
        public (ActionResult ActionResult, decimal TaxRate) TaxRateByLocation(GenericLocation genericLocation)
        {
            var client = new TaxJarHttpClient(Settings.TaxJarUrl);

            var result = client.TaxRateByLocation(genericLocation);

            return result;
        }

        public (ActionResult TotalTaxesActionResult, TotalCalculatedTaxes TotalTaxes) TotalTaxesForOrder(GenericLocation genericLocation, decimal orderGrossNoShipping)
        {
            var client = new TaxJarHttpClient(Settings.TaxJarUrl);

            var (totalTaxesActionResult, totalTaxes) = client.TotalTaxesForOrder(TaxJarLocation.ConvertFrom(genericLocation), orderGrossNoShipping);

            var calculatedTaxes = new TotalCalculatedTaxes()
            {
                TaxRate = totalTaxes.Rate,
                OrderGrossNoShipping = orderGrossNoShipping,
                OrderTaxes = totalTaxes.AmountToCollect

            };

            return (totalTaxesActionResult, calculatedTaxes);
        }
    }
}
