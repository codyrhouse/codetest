﻿using CodeTest.EndpointClients.TaxJar.Requests;
using CodeTest.EndpointClients.TaxJar.Responses;
using CodeTest.Pocos;
using CodeTest.Pocos.Locations;
using Flurl;
using System;
using Utils;

namespace CodeTest.EndpointClients.TaxJar
{
    public class TaxJarHttpClient
    {
        private readonly string _baseUrl;

        public TaxJarHttpClient(string baseUrl)
        {
            var baseUrlNull = Guard.IsStringNull(baseUrl, nameof(baseUrl));
            if (baseUrlNull.AssertionTrue)
            {
                throw new ArgumentNullException(baseUrlNull.ErrorMessage);
            }

            _baseUrl = baseUrl;
        }

        public (ActionResult ActionResult, decimal TaxRate) TaxRateByLocation(GenericLocation genericLocation)
        {
            var taxJarLocation = TaxJarLocation.ConvertFrom(genericLocation);

            return TaxRateByLocation(taxJarLocation);
        }

        public (ActionResult ActionResult, decimal TaxRate) TaxRateByLocation(TaxJarLocation taxJarLocation)
        {
            var locationCheck = taxJarLocation.IsValid();
            if (locationCheck.Success is false)
            {
                return (locationCheck, decimal.MinusOne);
            }

            var builtUrl = HttpClientHelper.BuildUrlWithQueryStrings(_baseUrl, "rates", (query) =>
            {
                // crh: could have used reflection on the class but felt like overkill here
                query[nameof(TaxJarLocation.Street).ToLower()] = taxJarLocation.Street;
                query[nameof(TaxJarLocation.City).ToLower()] = taxJarLocation.City;
                query[nameof(TaxJarLocation.State).ToLower()] = taxJarLocation.State;
                query[nameof(TaxJarLocation.Zip).ToLower()] = taxJarLocation.Zip;
                query[nameof(TaxJarLocation.Country).ToLower()] = taxJarLocation.Country;
            });

            var rate = HttpClientHelper.GetJson<TaxJarRateResponse, TaxJarClientExceptionResult>(builtUrl, Settings.TaxJarBearerToken);

            return (new ActionResult(true), rate.Rate.CombinedRate);
        }

        public (ActionResult ActionResult, TaxJarOrderSalesTaxResponse TotalTaxesResponse) TotalTaxesForOrder(TaxJarLocation taxJarLocation, decimal orderGrossNoShipping)
        {
            return TotalTaxesForOrder(TaxJarTaxesRequest.ConvertFrom(taxJarLocation), orderGrossNoShipping);
        }

        public (ActionResult ActionResult, TaxJarOrderSalesTaxResponse TotalTaxesResponse) TotalTaxesForOrder(TaxJarTaxesRequest taxesRequest, decimal orderGrossNoShipping)
        {
            taxesRequest.Amount = orderGrossNoShipping;
            taxesRequest.Shipping = 0;

            var validResult = taxesRequest.IsValid();

            if (validResult.Success is false)
            {
                return (validResult, new TaxJarOrderSalesTaxResponse());
            }

            var url = Url.Combine(Settings.TaxJarUrl, "taxes");

            var response = HttpClientHelper.PostJson<TaxJarTaxesRequest, TaxJarClientExceptionResult, TaxJarTaxesResponse>(taxesRequest, url, Settings.TaxJarBearerToken);

            return (new ActionResult(true), response.Tax);

        }
    }
}
