﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace CodeTest.EndpointClients.TaxJar.Responses
{
    [ExcludeFromCodeCoverage]
    public class TaxJarOrderSalesTaxResponse
    {
        [JsonProperty("amount_to_collect")]
        public decimal AmountToCollect { get; set; }

        [JsonProperty("rate")]
        public decimal Rate { get; set; }
    }
}
