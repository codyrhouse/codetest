﻿using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace CodeTest.EndpointClients.TaxJar.Responses
{
    [ExcludeFromCodeCoverage]
    public class TaxJarTaxesResponse
    {
        [JsonProperty("tax")]
        public TaxJarOrderSalesTaxResponse Tax { get; set; }

        [JsonProperty("order_total_amount")]
        public decimal OrderTotalAmount { get; set; }

        [JsonProperty("rate")]
        public decimal Rate { get; set; }

    }
}
