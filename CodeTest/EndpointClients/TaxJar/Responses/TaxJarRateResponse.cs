﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace CodeTest.EndpointClients.TaxJar.Responses
{
    [ExcludeFromCodeCoverage]
    public class TaxJarRateResponse
    {
        [JsonProperty("rate")]
        public TaxJarRate Rate { get; set; }


    }
}
