﻿using CodeTest.Interfaces;
using Utils;

namespace CodeTest.Pocos.Locations
{
    public class TaxJarLocation : IValidate
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }

        public ActionResult IsValid()
        {
            SanitizeData();
            var collector = new GuardResultCollector();

            collector.AddGuardCheck(Guard.IsZipCodeCorrect(Zip, nameof(Zip)));

            return ActionResult.Flatten(collector);
        }

        public void SanitizeData()
        {
            Street = Sanitizer.CleanString(Street, nameof(Street));
            City = Sanitizer.CleanString(City, nameof(City));
            State = Sanitizer.CleanString(State, nameof(State));
            Zip = Sanitizer.CleanString(Zip, nameof(Zip));
            Country = Sanitizer.CleanString(Country, nameof(Country));
        }

        public static TaxJarLocation ConvertFrom(GenericLocation genericLocation)
        {
            var loc = new TaxJarLocation();

            loc.Street = genericLocation.Street;
            loc.City = genericLocation.City;
            loc.State = genericLocation.State;
            loc.Zip = genericLocation.Zip;
            loc.Country = genericLocation.Country;

            loc.SanitizeData();

            return loc;
        }
    }
}
