﻿using CodeTest.Interfaces;
using CodeTest.Pocos.Locations;
using Newtonsoft.Json;
using Utils;

namespace CodeTest.EndpointClients.TaxJar.Requests
{
    public class TaxJarTaxesRequest : IValidate
    {
        [JsonProperty("from_city")]
        public string FromCity { get; set; } = string.Empty;

        [JsonProperty("from_state")]
        public string FromState { get; set; } = string.Empty;

        [JsonProperty("from_zip")]
        public string FromZip { get; set; } = string.Empty;

        [JsonProperty("from_country")]
        public string FromCountry { get; set; } = string.Empty;

        [JsonProperty("to_city")]
        public string ToCity { get; set; } = string.Empty;

        [JsonProperty("to_state")]
        public string ToState { get; set; } = string.Empty;

        [JsonProperty("to_zip")]
        public string ToZip { get; set; } = string.Empty;

        [JsonProperty("to_country")]
        public string ToCountry { get; set; } = string.Empty;

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("shipping")]
        public decimal Shipping { get; set; }

        public ActionResult IsValid()
        {
            SanitizeData();
            var collector = new GuardResultCollector();

            collector.AddGuardCheck(Guard.IsStringNotNull(FromCity, nameof(FromCity)));
            collector.AddGuardCheck(Guard.IsStringNotNull(ToCity, nameof(ToCity)));

            collector.AddGuardCheck(Guard.IsZipCodeCorrect(FromZip, nameof(FromZip)));
            collector.AddGuardCheck(Guard.IsZipCodeCorrect(ToZip, nameof(ToZip)));

            collector.AddGuardCheck(Guard.IsStringCorrectLength(FromState, nameof(FromState), 2));
            collector.AddGuardCheck(Guard.IsStringCorrectLength(ToState, nameof(ToState), 2));

            collector.AddGuardCheck(Guard.IsStringCorrectLength(FromCountry, nameof(FromCountry), 2));
            collector.AddGuardCheck(Guard.IsStringCorrectLength(ToCountry, nameof(ToCountry), 2));

            collector.AddGuardCheck(Guard.IsGreaterThanZero(Amount, nameof(Amount)));

            return ActionResult.Flatten(collector);

        }

        public void SanitizeData()
        {
            FromCity = Sanitizer.CleanString(FromCity, nameof(FromCity));
            FromState = Sanitizer.CleanString(FromState, nameof(FromState));
            FromZip = Sanitizer.CleanString(FromZip, nameof(FromZip));
            FromCountry = Sanitizer.CleanString(FromCountry, nameof(FromCountry));

            ToCity = Sanitizer.CleanString(ToCity, nameof(ToCity));
            ToState = Sanitizer.CleanString(ToState, nameof(ToState));
            ToZip = Sanitizer.CleanString(ToZip, nameof(ToZip));
            ToCountry = Sanitizer.CleanString(ToCountry, nameof(ToCountry));
        }

        public static TaxJarTaxesRequest ConvertFrom(TaxJarLocation genericLocation)
        {
            var loc = new TaxJarTaxesRequest();

            loc.FromCity = genericLocation.City;
            loc.FromState = genericLocation.State;
            loc.FromZip = genericLocation.Zip;
            loc.FromCountry = genericLocation.Country;

            loc.ToCity = genericLocation.City;
            loc.ToState = genericLocation.State;
            loc.ToZip = genericLocation.Zip;
            loc.ToCountry = genericLocation.Country;

            return loc;
        }
    }
}
