﻿using Newtonsoft.Json;
using Utils.Interfaces;

namespace CodeTest.EndpointClients.TaxJar
{
    public class TaxJarClientExceptionResult : IClientExceptionResult
    {
        public string Message => $"{Status} {Error}\r\n{Detail}";

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("detail")]
        public string Detail { get; set; }
    }
}
