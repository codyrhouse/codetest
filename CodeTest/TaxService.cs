﻿using CodeTest.Interfaces;
using CodeTest.Pocos;
using System;
using Utils;

namespace CodeTest
{
    public class TaxService : ITaxService
    {
        private readonly ITaxCalculator _taxCalculator;

        public TaxService(ITaxCalculator taxCalculator)
        {
            if (Guard.IsNotNull(taxCalculator, nameof(taxCalculator)).AssertionTrue is false)
            {
                throw new ArgumentNullException(nameof(taxCalculator));
            }

            _taxCalculator = taxCalculator;
        }

        public (ActionResult ActionResult, decimal TaxRate) TaxRateByLocation(GenericLocation genericLocation)
        {
            try
            {
                return _taxCalculator.TaxRateByLocation(genericLocation);
            }
            catch (Exception ex)
            {
                return (new ActionResult(ex), decimal.MinusOne);
            }
        }

        public (ActionResult TotalTaxesForOrderResult, TotalCalculatedTaxes TotalTaxes) TotalTaxesForOrder(GenericLocation genericLocation, decimal orderGrossNoShipping)
        {
            try
            {
                return _taxCalculator.TotalTaxesForOrder(genericLocation, orderGrossNoShipping);
            }
            catch (Exception ex)
            {
                return (new ActionResult(ex), TotalCalculatedTaxes.Empty);
            }

        }
    }
}
