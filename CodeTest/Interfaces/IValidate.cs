﻿using Utils;

namespace CodeTest.Interfaces
{
    public interface IValidate
    {
        ActionResult IsValid();
        void SanitizeData();
    }
}
