﻿using CodeTest.Pocos;
using Utils;

namespace CodeTest.Interfaces
{
    public interface ITaxCalculator
    {
        (ActionResult ActionResult, decimal TaxRate) TaxRateByLocation(GenericLocation genericLocation);
        (ActionResult TotalTaxesActionResult, TotalCalculatedTaxes TotalTaxes) TotalTaxesForOrder(GenericLocation genericLocation, decimal orderGrossNoShipping);
    }
}
