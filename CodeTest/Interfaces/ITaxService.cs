﻿using CodeTest.Pocos;
using Utils;

namespace CodeTest
{
    public interface ITaxService
    {
        (ActionResult ActionResult, decimal TaxRate) TaxRateByLocation(GenericLocation genericLocation);
        (ActionResult TotalTaxesForOrderResult, TotalCalculatedTaxes TotalTaxes) TotalTaxesForOrder(GenericLocation genericLocation, decimal orderGrossNoShipping);
    }
}
