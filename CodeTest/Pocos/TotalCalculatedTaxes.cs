﻿using System;

namespace CodeTest.Pocos
{
    public class TotalCalculatedTaxes
    {
        public decimal TaxRate { get; set; }
        public decimal OrderGrossNoShipping { get; set; }

        public decimal OrderTaxes { get; set; }

        public decimal OrderGrossNoShippingWithTaxes => Math.Round((TaxRate * OrderGrossNoShipping) + OrderGrossNoShipping, 2);

        public static TotalCalculatedTaxes Empty => new TotalCalculatedTaxes();
    }
}
